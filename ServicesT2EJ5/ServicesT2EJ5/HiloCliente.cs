﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace ServicesT2EJ5
{
    class HiloCliente
    {
        private Socket socketCliente;
        private NetworkStream ns;
        private StreamReader reader;
        private StreamWriter writer;
        public bool running = true;



        public HiloCliente(Socket socketCliente)
        {
            this.socketCliente = socketCliente;
        }

        public void run()
        {
            while (this.running)
            {
                using (this.ns = new NetworkStream(this.socketCliente))
                using (this.reader = new StreamReader(ns))
                using (this.writer = new StreamWriter(ns))
                {
                    while (this.running)
                    {
                        this.writer.WriteLine("Welcome!" + Environment.NewLine + "Commands:" +
                        Environment.NewLine + "get (filename)" +
                        Environment.NewLine + "list"
                        + Environment.NewLine + "upload (file)"
                        + Environment.NewLine + "exit" +
                        Environment.NewLine + "close");
                        this.writer.Flush();
                        String command = this.reader.ReadLine();

                        try { 
                            if (command != null)
                            {
                                if (command.ToLower().Trim().Contains("get"))
                                {
                                    String filename = command.Replace("get ","");
                                    lock (Program.l)
                                    {
                                        try
                                        {
                                            foreach (FileInfo f in Program.carpeta.GetFiles(filename))
                                            {
                                                using (StreamReader reader = new StreamReader(f.FullName))
                                                {
                                                    int leido;
                                                    do
                                                    {
                                                        leido = reader.Read();
                                                        if (leido > -1)
                                                        {
                                                            this.writer.Write((char)leido);
                                                        }
                                                        Console.WriteLine((char)leido);
                                                    } while (leido > -1);
                                                    this.writer.Flush();

                                                }
                                            }

                                        }
                                        catch (EndOfStreamException e)
                                        {

                                        }
                                        catch (Exception e) when (e is IOException | e is ArgumentException)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                    }
                                }else if (command.ToLower().Trim().Contains("upload"))
                                {
                                    string fileName = command.ToLower().Trim().Replace("upload ", "");
                                    string serverFile = Program.carpeta + "" + Path.DirectorySeparatorChar+fileName.Substring(fileName.LastIndexOf('\\'),fileName.Length- fileName.LastIndexOf('\\'));
                                    Console.WriteLine(fileName);
                                    Console.WriteLine(serverFile);
                                    lock (Program.l)
                                    {
                                        using (BinaryReader localReader = new BinaryReader(File.OpenRead(fileName)))
                                        using (BinaryWriter serverWriter = new BinaryWriter(File.OpenWrite(serverFile)))
                                        {
                                            byte[] buffer = new byte[50];
                                            int leido;
                                            do
                                            {
                                                leido = localReader.Read(buffer, 0, buffer.Length);
                                                if (leido > 0)
                                                {
                                                    serverWriter.Write(buffer, 0, buffer.Length);
                                                }
                                            } while (leido > 0);
                                        }
                                    }
                                }
                                else
                                {
                                    switch (command.ToLower().Trim())
                                    {
                                        case "list":
                                            FileInfo[] archivos = null;

                                            lock (Program.l)
                                            {
                                                archivos = Program.carpeta.GetFiles();
                                            }

                                            if (archivos != null)
                                            {
                                                if (archivos.Length > 0)
                                                {
                                                    this.writer.WriteLine();
                                                    this.writer.WriteLine("FILES:");
                                                    this.writer.Flush();
                                                    foreach (FileInfo f in archivos)
                                                    {
                                                        this.writer.WriteLine(f.Name);
                                                        this.writer.Flush();
                                                    }
                                                    this.writer.WriteLine();
                                                    this.writer.Flush();

                                                }
                                                else
                                                {
                                                    this.writer.WriteLine("No files found!");
                                                    this.writer.Flush();
                                                }
                                            }
                                            else
                                            {
                                                this.writer.WriteLine("Resource beeing used by another client, wait a second please!");
                                                this.writer.Flush();
                                            }
                                            break;

                                        case "close":
                                            lock (Program.l)
                                            {
                                                Program.running = false;
                                                Program.serverSocket.Close();
                                            }
                                            this.running = false;
                                            break;

                                        case "exit":
                                            this.running = false;
                                            break;

                                        default:
                                            this.writer.WriteLine("Command not recognised!");
                                            this.writer.Flush();
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                this.running = false;
                            }
                        }catch (IOException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        catch (Exception e) when (e is SocketException)
                        {
                            this.running = false;
                            this.socketCliente.Close();
                        }
                    }
                    this.socketCliente.Close();
                }
                
            }
        }
    }
}
