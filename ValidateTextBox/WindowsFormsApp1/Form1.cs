﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ValidateTextBox;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private ArrayList frikis = new ArrayList();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine(this.listBox1.SelectedIndices.Count - 1);
            for (int i = this.listBox1.SelectedIndices.Count-1;i >= 0;i--)
            {
                this.listBox1.Items.RemoveAt(i);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            DialogResult res = f.ShowDialog();
            if (res == DialogResult.OK)
            {
                sFriki fr = new sFriki();
                fr.Nombre = f.validateTextBoxNombre.Texto;
                fr.Edad = int.Parse(f.validateTextBoxEdad.Texto);
                fr.Sexo = f.sexo;
                fr.SexoOpuesto = f.sexoOpuesto;
                fr.Aficionprincipal = f.aficion;
                this.frikis.Add(fr);
                this.listBox1.Items.Add(fr.Nombre);
            }
        }
    }
}
