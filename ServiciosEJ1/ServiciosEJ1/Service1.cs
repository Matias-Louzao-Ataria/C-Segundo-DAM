﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace ServiciosEJ1
{
    public partial class ServiciosEJ1 : ServiceBase
    {
        public ServiciosEJ1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            int port = 2609;
            Server server = new Server();
            FileInfo configFile = new FileInfo(Environment.GetEnvironmentVariable("PROGRAMDATA") + "\\config.txt");
            if (configFile.Exists)
            {
                using (StreamReader reader = new StreamReader(configFile.FullName))
                {
                    try
                    {
                        port = int.Parse(reader.ReadLine());
                    }
                    catch (Exception e) when (e is ArgumentNullException || e is FormatException || e is OverflowException ||e is IOException || e is OutOfMemoryException)
                    {
                        this.EventWritter("Error reading config file, using deafult port!");
                    }
                }
            }
            else
            {
                this.EventWritter("No config file found, using deafult port!");
            }

            try
            {
                Thread thread = new Thread(new ThreadStart(() => { server.InitServer(port); }));
                thread.Start();
                this.EventWritter(string.Format("Running on port {0}", port));
            }
            catch (Exception e) when(e is OutOfMemoryException || e is ArgumentNullException ||e is ThreadStateException)
            {
                this.EventWritter("Failed to run service");
            }
            
        }

        private void EventWritter(string msg)
        {
            string serviceName = "Número más alto";
            string destination = "Application";

            if (!EventLog.SourceExists(serviceName))
            {
                EventLog.CreateEventSource(serviceName,destination);
            }

            EventLog.WriteEntry(msg);
        }

        protected override void OnStop()
        {
            Server.running = false;
        }
    }
}
