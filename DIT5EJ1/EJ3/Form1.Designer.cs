﻿
namespace EJ3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.reproductor1 = new DIT5EJ1.Reproductor();
            this.labelTextbox1 = new DIT5EJ1.LabelTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // reproductor1
            // 
            this.reproductor1.Location = new System.Drawing.Point(43, 76);
            this.reproductor1.Name = "reproductor1";
            this.reproductor1.Size = new System.Drawing.Size(78, 76);
            this.reproductor1.TabIndex = 0;
            this.reproductor1.X = 0;
            this.reproductor1.Y = 56;
            this.reproductor1.DesbordaTiempo += new System.EventHandler(this.DesbordamientoTest);
            this.reproductor1.botonPulsado += new System.EventHandler(this.reproductor1_botonPulsado);
            // 
            // labelTextbox1
            // 
            this.labelTextbox1.Location = new System.Drawing.Point(185, 132);
            this.labelTextbox1.Name = "labelTextbox1";
            this.labelTextbox1.PasswdChar = '\0';
            this.labelTextbox1.Posicion = DIT5EJ1.LabelTextbox.ePosicion.IZQUIERDA;
            this.labelTextbox1.Separacion = 0;
            this.labelTextbox1.Size = new System.Drawing.Size(150, 20);
            this.labelTextbox1.TabIndex = 1;
            this.labelTextbox1.TextLbl = "Images directory";
            this.labelTextbox1.TextTxt = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(386, 76);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 259);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelTextbox1);
            this.Controls.Add(this.reproductor1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DIT5EJ1.Reproductor reproductor1;
        private DIT5EJ1.LabelTextbox labelTextbox1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

