﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace ServicesT2EJ5
{
    class Program
    {
        public static DirectoryInfo carpeta = new DirectoryInfo(Environment.SpecialFolder.ApplicationData + ""+Path.DirectorySeparatorChar + "Archivos");
        public static bool running = true;
        public static Object l = new Object();
        public static Socket serverSocket;
        static void Main(string[] args)
        {
            try
            {
                if (!carpeta.Exists)
                {
                    carpeta.Create();
                }
                IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Any, 2609);
                serverSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(iPEndPoint);
                serverSocket.Listen(10);
                while (running)
                {
                    Socket socketCliente = serverSocket.Accept();
                    Thread hiloCliente = new Thread(() => { HiloCliente cliente = new HiloCliente(socketCliente); cliente.run(); });
                    hiloCliente.Start();
                }
            }
            catch (SocketException e) when (e.SocketErrorCode == SocketError.AddressAlreadyInUse)
            {
                Console.WriteLine("Address already in use!");
            }
            catch(Exception e) when (e is SocketException |e is IOException)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
