﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ValidateTextBox;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public sFriki.eSexo sexo;
        public sFriki.eSexo sexoOpuesto;
        public sFriki.eAficion aficion;

        public Form2()
        {
            InitializeComponent();
            this.radioButton1.Tag = sFriki.eSexo.Hombre;
            this.radioButton2.Tag = sFriki.eSexo.Mujer;
            this.radioButton3.Tag = sFriki.eSexo.Hombre;
            this.radioButton4.Tag = sFriki.eSexo.Mujer;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton r = ((RadioButton)sender);
            if (r.Checked)
            {
                this.sexoOpuesto = (sFriki.eSexo)r.Tag;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton r = ((RadioButton)sender);
            if (r.Checked)
            {
                this.sexo = (sFriki.eSexo)r.Tag;
            }
        }

        private void comboBoxAficion_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.aficion = (sFriki.eAficion)this.comboBoxAficion.SelectedIndex;
        }
    }
}
