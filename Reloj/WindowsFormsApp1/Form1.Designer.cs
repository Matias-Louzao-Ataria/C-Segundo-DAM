﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.reloj1 = new Reloj.Reloj();
            this.SuspendLayout();
            // 
            // reloj1
            // 
            this.reloj1.Hours = 0;
            this.reloj1.Location = new System.Drawing.Point(199, 76);
            this.reloj1.Minits = 0;
            this.reloj1.Name = "reloj1";
            this.reloj1.Secs = 0;
            this.reloj1.Size = new System.Drawing.Size(322, 322);
            this.reloj1.TabIndex = 0;
            this.reloj1.Text = "reloj1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reloj1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Reloj.Reloj reloj1;
    }
}

