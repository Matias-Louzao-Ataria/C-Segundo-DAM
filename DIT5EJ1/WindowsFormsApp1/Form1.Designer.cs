﻿namespace DIT5EJ1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.reproductor1 = new DIT5EJ1.Reproductor();
            this.etiquetaAviso1 = new DIT5EJ1.EtiquetaAviso();
            this.labelTextbox1 = new DIT5EJ1.LabelTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(43, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(171, 59);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(304, 59);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // reproductor1
            // 
            this.reproductor1.Location = new System.Drawing.Point(168, 207);
            this.reproductor1.Name = "reproductor1";
            this.reproductor1.Size = new System.Drawing.Size(78, 76);
            this.reproductor1.TabIndex = 6;
            // 
            // etiquetaAviso1
            // 
            this.etiquetaAviso1.Adorno = DIT5EJ1.EtiquetaAviso.eAdorno.Imagen;
            this.etiquetaAviso1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.etiquetaAviso1.Gradient = true;
            this.etiquetaAviso1.GradientEnd = System.Drawing.Color.MediumVioletRed;
            this.etiquetaAviso1.GradientStart = System.Drawing.Color.RoyalBlue;
            this.etiquetaAviso1.ImagenMarca = ((System.Drawing.Bitmap)(resources.GetObject("etiquetaAviso1.ImagenMarca")));
            this.etiquetaAviso1.Location = new System.Drawing.Point(534, 34);
            this.etiquetaAviso1.Name = "etiquetaAviso1";
            this.etiquetaAviso1.Size = new System.Drawing.Size(110, 48);
            this.etiquetaAviso1.TabIndex = 5;
            this.etiquetaAviso1.Text = "etiquetaAviso1";
            this.etiquetaAviso1.ClickEnMarca += new System.EventHandler(this.EtiquetaAviso1_ClickEnMarca);
            // 
            // labelTextbox1
            // 
            this.labelTextbox1.Location = new System.Drawing.Point(43, 181);
            this.labelTextbox1.Name = "labelTextbox1";
            this.labelTextbox1.PasswdChar = '*';
            this.labelTextbox1.Posicion = DIT5EJ1.LabelTextbox.ePosicion.IZQUIERDA;
            this.labelTextbox1.Separacion = 0;
            this.labelTextbox1.Size = new System.Drawing.Size(151, 20);
            this.labelTextbox1.TabIndex = 2;
            this.labelTextbox1.TextLbl = "LablTextbox";
            this.labelTextbox1.TextTxt = "";
            this.labelTextbox1.CambiaPosicion += new System.EventHandler(this.PositionChanged);
            this.labelTextbox1.CambiarSeparacion += new System.EventHandler(this.SeparationChanged);
            this.labelTextbox1.TxtTextChanged += new System.EventHandler(this.Button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(329, 149);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(169, 134);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.reproductor1);
            this.Controls.Add(this.etiquetaAviso1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.labelTextbox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

         
        private System.Windows.Forms.Button button1;
        private LabelTextbox labelTextbox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private EtiquetaAviso etiquetaAviso1;
        private Reproductor reproductor1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}