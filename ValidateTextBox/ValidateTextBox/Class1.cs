﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidateTextBox
{
    public class Class1
    {
    }

    public struct sFriki
    {
        private string nombre;
        private int edad;
        private eAficion aficionPrincipal;
        private eSexo sexo;
        private eSexo sexoOpuesto;
        private string foto;

        public eSexo Sexo { get; set; }

        public string Nombre
        {
            set
            {
                this.nombre = value;
            }

            get
            {
                return this.nombre;
            }
        }

        public int Edad
        {
            set
            {
                this.edad = value;
            }

            get
            {
                return this.edad;
            }
        }

        public eAficion Aficionprincipal
        {
            set
            {
                this.aficionPrincipal = value;
            }

            get
            {
                return this.aficionPrincipal;
            }
        }

        public eSexo SexoOpuesto
        {
            set
            {
                this.sexoOpuesto = value;
            }

            get
            {
                return this.sexoOpuesto;
            }
        }

        public string Foto
        {
            set
            {
                this.Foto = value;
            }


            get 
            {
                return this.foto;
            }
        }

        public enum eSexo
        {
            Hombre,
            Mujer
        }

        public enum eAficion
        {
            Manga = 0,
            SciFi = 1,
            RPG = 2,
            Fantasia = 3,
            Terror = 4,
            Tecnologia = 5
        }
    }
}
