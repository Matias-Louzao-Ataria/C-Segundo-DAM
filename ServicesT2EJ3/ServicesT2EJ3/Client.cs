using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace ServiciosEJ1
{
    class Client
    {
        private Socket socket = null;
        private NetworkStream ns = null;
        private StreamReader reader = null;
        private StreamWriter writer = null;
        bool running = true;
        string username = "";
        string fullUsername = "";
        string ip = null;
        public int num = -1;

        public void run(Socket clientSocket)
        {
            try
            {
                this.socket = clientSocket;
                IPEndPoint iPEndClient = (IPEndPoint)clientSocket.RemoteEndPoint;
                this.ip = iPEndClient.Address.ToString();
                using (this.ns = new NetworkStream(clientSocket))
                using (this.reader = new StreamReader(ns))
                using (this.writer = new StreamWriter(ns))
                {
                    while (this.username == "" || this.username == null && !Server.players.Contains(this))
                    {
                        this.writer.WriteLine("Enter a username!");
                        this.writer.Flush();
                        this.username = this.reader.ReadLine();
                        lock (Server.l)
                        {
                            if (Server.players.Count <= 0)
                            {
                                Server.players.Add(this);
                            }
                            else if (Server.players.Contains(this))
                            {
                                this.writer.WriteLine("Player already playing, choose a diferent username!");
                                this.writer.Flush();
                                this.username = "";
                            }
                            else if (!Server.players.Contains(this))
                            {
                                Server.players.Add(this);
                            }
                        }
                    }
                    this.writer.WriteLine("You've entered the game!");
                    this.writer.Flush();
                    this.fullUsername = this.username + "@" + this.ip;
                    this.PassMsg(this.fullUsername + " entered the game",false);
                    while (this.running)
                    {
                        if (Server.players.Count <= 1)
                        {
                            this.writer.WriteLine("Waiting for more players!");
                            this.writer.Flush();
                            lock (Server.l)
                            {
                                Monitor.Wait(Server.l);
                            }
                        }
                        else
                        {
                            lock (Server.l)
                            {
                                Monitor.Pulse(Server.l);
                            }
                        }

                        lock (Server.l)
                        {
                            this.num = Server.randomN.Next(0, 21);
                        }
                        Console.WriteLine("{0} : {1}", this.username, this.num);
                    
                        while (this.running && Server.players.Count > 1)
                        {
                            if (Server.contador == null)
                            {
                                Thread counter = new Thread(() =>
                                {
                                    while (Server.countDown > -1)
                                    {
                                        Console.WriteLine(Server.countDown);
                                        if (Server.countDown > 0)
                                        {
                                            Thread.Sleep(1000);
                                            PassMsg(string.Format("{0} seconds remaining!", Server.countDown),true);
                                            Server.countDown--;
                                        }
                                        else
                                        {
                                            ArrayList winner = CheckWinner();
                                            foreach (Client c in winner)
                                            {
                                                if (winner.Count > 1)
                                                {
                                                    if (winner.IndexOf(c) == 0)
                                                    {
                                                        PassMsg(string.Format("Empate entre:"), true);
                                                    }
                                                    PassMsg(c.fullUsername, true);
                                                }
                                                else
                                                {
                                                    PassMsg(string.Format("the winner is: {0}!", c.fullUsername), true);
                                                }
                                            }
                                            Server.countDown--;
                                            lock (Server.l)
                                            {
                                                Monitor.PulseAll(Server.l);
                                            }
                                        }
                                    }
                                }
                                );
                                Server.contador = counter;
                                counter.Start();
                            }
                            else
                            {
                                if (Server.countDown <= 0)
                                {
                                    this.running = false;
                                    this.writer.WriteLine("Game start!"+Environment.NewLine+" your number is: "+this.num);
                                    this.writer.Flush();
                                    lock (Server.l)
                                    {
                                        Monitor.Wait(Server.l);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!this.running)
                {
                    lock (Server.l)
                    {
                        Server.players.Remove(this);
                        
                    }
                    this.socket.Close();
                }
            }
            catch (Exception ex) when (ex is SocketException || ex is IOException)
            {
                Console.WriteLine("ERROR client thread!");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                lock (Server.l)
                {
                    Server.players.Remove(this);
                }
            }
        }

        private void PassMsg(string msg,bool toAll)
        {
            lock (Server.l)
            {
                
                foreach (Client c in Server.players)
                {
                    if (c.writer != null && c != this || toAll && c.writer != null)
                    {
                        try
                        {
                            c.writer.WriteLine(msg);
                            c.writer.Flush();
                        }
                        catch (Exception ex) when (ex is IOException || ex is SocketException || ex is ObjectDisposedException)
                        {
                            Console.WriteLine("ERROR writting to client!");
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                
            }
        }

        private ArrayList CheckWinner()
        {
            int max = 0;
            ArrayList winner = new ArrayList();
            lock (Server.l)
            {
                foreach (Client c in Server.players)
                {
                    Console.WriteLine("Players: "+ Server.players.Count);
                    if (c.num > max)
                    {
                        max = c.num;
                        winner.Add(c);
                    }else if (c.num == max)
                    {
                        winner.Add(c);
                    }
                }
            }
            return winner;
        }

        public override bool Equals(object obj)
        {
            try
            {
                Client cl = (Client)obj;
                return this.username == cl.username && this.ip == cl.ip;
            }
            catch (Exception ex) when (ex is InvalidCastException)
            {
                return false;
            }
        }
    }
}
