﻿using System;
using System.Collections;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace ServicesT2EJ4
{
    class Client
    {
        private bool running = true;
        private NetworkStream ns;
        private StreamWriter writer;
        private StreamReader reader;

        /*
         * 0.-General error
         * 1.-Correct
         * 2.-Syntax Error
         * 3.-No content
         */

        public void run(Socket clientScocket)
        {
            string command = "";
            try
            {
                using (ns = new NetworkStream(clientScocket))
                using (writer = new StreamWriter(ns))
                using (reader = new StreamReader(ns))
                {
                    while (running)
                    {
                        command = reader.ReadLine();
                        if (command != null)
                        {
                            if (command.Contains("getword"))
                            {
                                lock (Program.wordsLock)
                                {
                                    ReadFile(Program.wordsFile);
                                }
                            }
                            else if (command.Contains("sendword"))
                            {
                                addContent(command,"sendword ",Program.wordsFile, Program.wordsLock);
                            }
                            else if (command.Contains("getrecords"))
                            {
                                lock (Program.recordsLock)
                                {
                                    ReadFile(Program.recordsFile);
                                }
                            }
                            else if (command.Contains("sendrecord"))
                            {
                                addContent(command,"sendrecord ",Program.recordsFile,Program.recordsLock);
                            }
                            else if (command.Contains("closeserver"))
                            {
                                string passwd = command.Replace("closeserver ", ""),readPasswd = "dummy",hashPasswd = "";
                                if (passwd.Trim().Replace("   ", "").Replace(" ", "").Length > 0 && passwd != command)
                                {
                                    using (SHA512 sha = SHA512.Create())
                                    {
                                        using (BinaryReader binaryReader = new BinaryReader(File.OpenRead(Program.passwordsFile.FullName)))
                                        {
                                            try
                                            {
                                                while (true && hashPasswd != readPasswd)
                                                {
                                                    readPasswd = binaryReader.ReadString();
                                                    if (readPasswd.Trim().Replace("   ", "").Replace(" ", "").Length > 0)
                                                    {
                                                        hashPasswd = BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(passwd))).Replace("-", "");
                                                        if (hashPasswd == readPasswd)
                                                        {
                                                            writer.WriteLine(1);//Confirmation
                                                            writer.Flush();
                                                            lock (Program.serverSocketLock)
                                                            {
                                                                Program.running = false;
                                                                Program.serverSocket.Close();
                                                            }
                                                            this.running = false;
                                                            clientScocket.Close();
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception e) when (e is EndOfStreamException)
                                            {
                                                Console.WriteLine("End of stream!");
                                            }

                                            if (hashPasswd != readPasswd)
                                            {
                                                writer.WriteLine(0);//Error confimation
                                                writer.Flush();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    writer.WriteLine(2);//Syntax error
                                    writer.Flush();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) when (e is SocketException || e is IOException)
            {
                lock (Program.wordsLock)
                {
                    Console.WriteLine("Client thread error!");
                    Console.WriteLine(e.Message);
                }
                this.running = false;
                clientScocket.Close();
            }
        }

        private void addContent(string command,string replace,FileInfo file,object l)
        {
            string newContent = command.Replace(replace, "");
            if (newContent.Trim().Replace("   ", "").Replace(" ", "").Length > 0 && newContent != command)
            {
                try
                {
                    lock (l)
                    {
                        if (file == Program.recordsFile)
                        {
                            int cont = 0;
                            string content = "", readContent = "";
                            IPAddress ip = null;

                            foreach (IPAddress i in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                            {
                                if (i.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    ip = i;
                                }
                            }

                            using (StreamReader streamReader = new StreamReader(file.FullName))
                            {
                                while ((readContent = streamReader.ReadLine()) != null)
                                {
                                    if (readContent.Trim().Replace("   ", "").Replace(" ", "").Length > 0)
                                    {
                                        content += readContent + Environment.NewLine;
                                        cont++;
                                    }
                                }
                            }
                            if (cont < 10)
                            {
                                using (StreamWriter fileWriter = new StreamWriter(file.FullName, true))
                                {
                                    fileWriter.WriteLine(newContent+" "+ip);
                                    writer.WriteLine(1);//Confirmation
                                    writer.Flush();
                                }
                            }
                            else
                            {
                                content = content.Remove(0, content.IndexOf(Environment.NewLine) + 2);//No sé proqué +2.
                                using (StreamWriter fileWriter2 = new StreamWriter(file.FullName))
                                {
                                    fileWriter2.Write(content);
                                    fileWriter2.WriteLine(newContent + " " + ip);
                                }
                                writer.WriteLine(1);//Confirmation
                                writer.Flush();
                            }

                        }
                        else
                        {
                            using (StreamWriter fileWriter = new StreamWriter(file.FullName, true))
                            {
                                fileWriter.WriteLine(newContent);
                                writer.WriteLine(1);//Confirmation
                                writer.Flush();
                            }
                        }
                    }
                }
                catch (Exception e) when (e is IOException)
                {
                    writer.WriteLine(0);//Error confirmation
                    writer.Flush();
                }
            }
            else
            {
                writer.WriteLine(2);//Syntax error
                writer.Flush();
            }
        }

        private void ReadFile(FileInfo file)
        {
            if (file.Length <= 0)
            {
                writer.WriteLine(3);//No content to read.
                writer.Flush();
            }
            else
            {
                using (StreamReader wordsReader = new StreamReader(file.FullName))
                {
                    try
                    {
                        string readword = "", words = "";
                        while ((readword = wordsReader.ReadLine()) != null)
                        {
                            if (readword.Trim().Replace("   ", "").Replace(" ", "").Length > 0)
                            {
                                words += readword + Environment.NewLine;
                            }
                        }
                        words = words.Remove(words.LastIndexOf(Environment.NewLine),1);
                        writer.WriteLine(words);
                        writer.Flush();
                    }
                    catch (Exception e) when (e is IOException)
                    {
                        Console.WriteLine(e.Message);
                        writer.WriteLine(0);//Error confirmation
                        writer.Flush();
                    }
                }
            }
        }
    }
}
