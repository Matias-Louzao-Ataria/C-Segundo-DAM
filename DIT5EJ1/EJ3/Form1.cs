﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EJ3
{
    public partial class Form1 : Form
    {
        private bool running = false;
        private ArrayList fotos = new ArrayList();
        private System.Windows.Forms.Timer t;
        private int contFoto = 0;
        private DirectoryInfo imgDir;

        public Form1()
        {
            InitializeComponent();
        }

        private void reproductor1_botonPulsado(object sender, EventArgs e)
        {
            this.running = !this.running;
            if (this.labelTextbox1.TextTxt.Length > 0 && running)
            {
                try
                {
                    if (imgDir != new DirectoryInfo(this.labelTextbox1.TextTxt))
                    {
                        imgDir = new DirectoryInfo(this.labelTextbox1.TextTxt);
                        this.fotos = new ArrayList();
                    }
                    foreach (FileInfo archivo in imgDir.GetFiles())
                    {
                        try
                        {
                            Bitmap foto = new Bitmap(archivo.FullName);
                            this.fotos.Add(foto);
                        }
                        catch (Exception ex) when (ex is FileNotFoundException || ex is ArgumentNullException || ex is ArgumentException)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                catch (Exception ex) when (ex is DirectoryNotFoundException)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            
            
            if (t == null)
            {
                t = new System.Windows.Forms.Timer();
                t.Interval = 1000;
                t.Tick += Tick;
            }
            t.Start();
        }

        private void Tick(Object sender, EventArgs e)
        {
            if (running)
            {
                this.reproductor1.Y++;
                Console.WriteLine(this.reproductor1.Y >= 59);
                if (this.reproductor1.Y >= 59)
                {
                    this.reproductor1.Y = 0;
                }
                if (this.fotos.Count > 0)
                {
                    if (contFoto >= this.fotos.Count)
                    {
                        contFoto = 0;
                    }
                    this.pictureBox1.Image = (Bitmap)this.fotos.ToArray()[contFoto];
                    contFoto++;
                }
            }
            else
            {
                t.Stop();
            }
        }

        private void DesbordamientoTest(Object sender,EventArgs e)
        {
            this.reproductor1.X++;
        }
    }
}
