﻿
namespace WindowsFormsApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.validateTextBoxNombre = new ValidateTextBox.ValidateTextBox();
            this.validateTextBoxEdad = new ValidateTextBox.ValidateTextBox();
            this.comboBoxAficion = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBoxSexo = new System.Windows.Forms.GroupBox();
            this.groupBoxSexoOpuesto = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBoxSexo.SuspendLayout();
            this.groupBoxSexoOpuesto.SuspendLayout();
            this.SuspendLayout();
            // 
            // validateTextBoxNombre
            // 
            this.validateTextBoxNombre.Location = new System.Drawing.Point(184, 89);
            this.validateTextBoxNombre.Multilinea = false;
            this.validateTextBoxNombre.Name = "validateTextBoxNombre";
            this.validateTextBoxNombre.Size = new System.Drawing.Size(150, 40);
            this.validateTextBoxNombre.TabIndex = 0;
            this.validateTextBoxNombre.Texto = "";
            this.validateTextBoxNombre.Tipo = ValidateTextBox.ValidateTextBox.eTipo.Textual;
            // 
            // validateTextBoxEdad
            // 
            this.validateTextBoxEdad.Location = new System.Drawing.Point(396, 89);
            this.validateTextBoxEdad.Multilinea = false;
            this.validateTextBoxEdad.Name = "validateTextBoxEdad";
            this.validateTextBoxEdad.Size = new System.Drawing.Size(150, 40);
            this.validateTextBoxEdad.TabIndex = 1;
            this.validateTextBoxEdad.Texto = "";
            this.validateTextBoxEdad.Tipo = ValidateTextBox.ValidateTextBox.eTipo.Numerico;
            // 
            // comboBoxAficion
            // 
            this.comboBoxAficion.FormattingEnabled = true;
            this.comboBoxAficion.Items.AddRange(new object[] {
            "Manga",
            "SciFi",
            "RPG",
            "Fantasia",
            "Terror",
            "Tecnologia"});
            this.comboBoxAficion.Location = new System.Drawing.Point(306, 178);
            this.comboBoxAficion.Name = "comboBoxAficion";
            this.comboBoxAficion.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAficion.TabIndex = 2;
            this.comboBoxAficion.SelectedIndexChanged += new System.EventHandler(this.comboBoxAficion_SelectedIndexChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 32);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(85, 17);
            this.radioButton1.TabIndex = 3;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 55);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(85, 17);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton2";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(16, 30);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(85, 17);
            this.radioButton3.TabIndex = 5;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "radioButton3";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(16, 53);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(85, 17);
            this.radioButton4.TabIndex = 6;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "radioButton4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // groupBoxSexo
            // 
            this.groupBoxSexo.Controls.Add(this.radioButton1);
            this.groupBoxSexo.Controls.Add(this.radioButton2);
            this.groupBoxSexo.Location = new System.Drawing.Point(134, 247);
            this.groupBoxSexo.Name = "groupBoxSexo";
            this.groupBoxSexo.Size = new System.Drawing.Size(200, 114);
            this.groupBoxSexo.TabIndex = 9;
            this.groupBoxSexo.TabStop = false;
            this.groupBoxSexo.Text = "Sexo";
            // 
            // groupBoxSexoOpuesto
            // 
            this.groupBoxSexoOpuesto.Controls.Add(this.radioButton3);
            this.groupBoxSexoOpuesto.Controls.Add(this.radioButton4);
            this.groupBoxSexoOpuesto.Location = new System.Drawing.Point(396, 247);
            this.groupBoxSexoOpuesto.Name = "groupBoxSexoOpuesto";
            this.groupBoxSexoOpuesto.Size = new System.Drawing.Size(200, 114);
            this.groupBoxSexoOpuesto.TabIndex = 10;
            this.groupBoxSexoOpuesto.TabStop = false;
            this.groupBoxSexoOpuesto.Text = "SexoOpuesto";
            this.groupBoxSexoOpuesto.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "jpg (*.jpg)| *.jpg";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(480, 176);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(610, 176);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AcceptButton = this.button2;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBoxSexoOpuesto);
            this.Controls.Add(this.groupBoxSexo);
            this.Controls.Add(this.comboBoxAficion);
            this.Controls.Add(this.validateTextBoxEdad);
            this.Controls.Add(this.validateTextBoxNombre);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBoxSexo.ResumeLayout(false);
            this.groupBoxSexo.PerformLayout();
            this.groupBoxSexoOpuesto.ResumeLayout(false);
            this.groupBoxSexoOpuesto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.GroupBox groupBoxSexo;
        public System.Windows.Forms.GroupBox groupBoxSexoOpuesto;
        public System.Windows.Forms.Button button1;
        public ValidateTextBox.ValidateTextBox validateTextBoxNombre;
        public ValidateTextBox.ValidateTextBox validateTextBoxEdad;
        public System.Windows.Forms.ComboBox comboBoxAficion;
        private System.Windows.Forms.Button button2;
    }
}