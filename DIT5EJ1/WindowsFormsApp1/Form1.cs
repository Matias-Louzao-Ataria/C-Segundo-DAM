﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DIT5EJ1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (this.labelTextbox1.Posicion != LabelTextbox.ePosicion.DERECHA)
            {
                this.labelTextbox1.Posicion = LabelTextbox.ePosicion.DERECHA;
            }
            else
            {
                this.labelTextbox1.Posicion = LabelTextbox.ePosicion.IZQUIERDA;
            }
        }

        private void PositionChanged(Object sender,EventArgs e)
        {
            LabelTextbox lbl = (LabelTextbox)sender;
            this.Text = lbl.Posicion.ToString();
        }


        private void SeparationChanged(Object sender,EventArgs e)
        {
            LabelTextbox lbl = (LabelTextbox)sender;
            this.Text = lbl.Separacion.ToString();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.labelTextbox1.Separacion++;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Txt text changed");
            try
            {
                this.etiquetaAviso1.ImagenMarca = ((Bitmap)null);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException || ex is NullReferenceException)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void EtiquetaAviso1_ClickEnMarca(object sender, EventArgs e)
        {
            Console.WriteLine("marca");
        }

        private void Reproductor1_textChanged(object sender, EventArgs e)
        {
            
        }
    }
}
