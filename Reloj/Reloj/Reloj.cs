﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reloj
{
    [DefaultProperty("Hour")]
    [DefaultEvent("Load")]
    public partial class Reloj : Control
    {
        private int hours, minits, secs;

        [Category("1")]
        [Description("Hours")]
        public int Hours { get; set; }

        [Category("1")]
        [Description("Minutes")]
        public int Minits { get; set; }

        [Category("1")]
        [Description("Seconds")]
        public int Secs { get; set; }


        public Reloj()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Width = this.Height;
            e.Graphics.FillEllipse(Brushes.White,0, 0, this.Height, this.Height);

            //12:00
            e.Graphics.TranslateTransform(this.Width/2,0);
            e.Graphics.DrawLine(Pens.Black,0,0,0,10);
            e.Graphics.ResetTransform();
            
            //6:00
            e.Graphics.TranslateTransform(this.Width/2,this.Height);
            e.Graphics.DrawLine(Pens.Black,0,0,0,-10);
            e.Graphics.ResetTransform();

            //3:00
            e.Graphics.TranslateTransform(this.Width,this.Height/2);
            e.Graphics.RotateTransform(90);
            e.Graphics.DrawLine(Pens.Black,0,0,0,10);
            e.Graphics.ResetTransform();

            //9:00
            e.Graphics.TranslateTransform(0,this.Height/2);
            e.Graphics.RotateTransform(-90);
            e.Graphics.DrawLine(Pens.Black,0,0,0,10);
            e.Graphics.ResetTransform();
        }
    }
}
