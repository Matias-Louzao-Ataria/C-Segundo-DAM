﻿using System;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace ServicesT2EJ4
{
    class Program
    {

        public static bool running = true;
        public static FileInfo wordsFile = new FileInfo("words.txt");
        public static FileInfo recordsFile = new FileInfo("records.txt");
        public static FileInfo passwordsFile = new FileInfo("passwords");
        public static Socket serverSocket = null;
        public static object wordsLock = new object();
        public static object recordsLock = new object();
        public static object serverSocketLock = new object();

        static void Main(string[] args)
        {
            try
            {
                if (!wordsFile.Exists)
                {
                    using (StreamWriter writer = new StreamWriter(wordsFile.FullName))
                    {

                    }
                }

                if (!recordsFile.Exists)
                {
                    using (StreamWriter writer = new StreamWriter(recordsFile.FullName))
                    {

                    }
                }

                if (!passwordsFile.Exists)
                {
                    Console.WriteLine("There are no passwords to close the server!"+Environment.NewLine+"Generating automatic one" + Environment.NewLine +"Automatic password is \"secret\".");
                    using (BinaryWriter binaryWriter = new BinaryWriter(File.OpenWrite(passwordsFile.FullName)))
                    {
                        using (SHA512 sha = SHA512.Create())
                        {
                            binaryWriter.Write(BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes("secret"))).Replace("-", ""));
                        }
                    }
                }
                
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 2609);
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(endPoint);
                serverSocket.Listen(10);
                while (running)
                {
                    Socket clientSocket = serverSocket.Accept();
                    Client c = new Client();
                    Thread t = new Thread(()=> { c.run(clientSocket); });
                    t.Start();
                }
            }
            catch (SocketException e) when (e is SocketException && ((SocketException)e).SocketErrorCode == SocketError.AddressAlreadyInUse)
            {
                Console.WriteLine("Address already in use!");
            }catch(Exception e) when (e is SocketException || e is IOException)
            {
                Console.WriteLine("Server error!");
                Console.WriteLine(e.Message);
            }
        }
    }
}
