﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using System.ServiceProcess;

namespace ServiciosEJ1
{
    class Server
    {
        public static bool running = true;
        public static Random randomN = new Random();
        public static ArrayList players = new ArrayList();
        public static Object l = new Object();
        private static int waitingTime = 5;
        public static int countDown = waitingTime;
        public static Thread contador = null;

        public void InitServer(int port)
        {
            try
            {
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Any, port);
                Socket serverSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(serverEndPoint);
                serverSocket.Listen(10);
                while (Server.running)
                {
                    Socket socketClient = serverSocket.Accept();
                    if (countDown <= 0)
                    {
                        Server.contador = null;
                        Server.countDown = waitingTime;
                    }
                    Client client = new Client();
                    Thread clientThread = new Thread(() => client.run(socketClient));
                    clientThread.Start();
                }

                lock (Server.l)
                {
                    foreach (Client c in Server.players)
                    {
                        c.running = false;
                        c.socket.Close();
                        Monitor.PulseAll(Server.l);
                    }
                }

                serverSocket.Close();
            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode.Equals(SocketError.AddressAlreadyInUse))
                {
                    EventWritter(ex, "Port already in use, finishing the process!");
                }
                else
                {
                    Console.WriteLine("Server error!");
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void EventWritter(SocketException ex, string msg)
        {
            string serviceName = "Número más alto";
            string destination = "Application";


            if (!EventLog.SourceExists(serviceName))
            {
                EventLog.CreateEventSource(serviceName, destination);
            }

            EventLog e = new EventLog(destination);
            e.WriteEntry(msg);
        }
    }
}
