﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ValidateTextBox
{
    [DefaultProperty("Text")]
    [DefaultEvent("Load")]
    public partial class ValidateTextBox : UserControl
    {
        public event EventHandler TextBoxTextChanged;
        private Brush brush = Brushes.Red;


        public ValidateTextBox()
        {
            InitializeComponent();
        }

        public enum eTipo
        {
            Numerico,
            Textual
        }

        private eTipo tipo;

        [Category("1")]
        [Description("Accepted data type")]
        public eTipo Tipo
        {
            set
            {
                this.tipo = value;
                CheckContent();
                this.Refresh();
            }

            get
            {
                return this.tipo;
            }
        }

        private void CheckContent()
        {
            if (this.tipo == eTipo.Numerico)
            {
                if (CheckNumber(this.textBox1.Text))
                {
                    this.brush = Brushes.Green;
                }
                else
                {
                    this.brush = Brushes.Red;
                }
            }
            else
            {
                if (CheckTextual(this.textBox1.Text))
                {
                    this.brush = Brushes.Green;
                }
                else
                {
                    this.brush = Brushes.Red;
                }
            }
        }

        [Category("1")]
        [Description("TextBox's text")]
        public string Texto
        {
            set
            {
                this.textBox1.Text = value;
            }

            get
            {
                return this.textBox1.Text;
            }
        }

        [Category("1")]
        [Description("TextBox's Multiline")]
        public bool Multilinea
        {
            set
            {
                this.textBox1.Multiline = value;
            }

            get
            {
                return this.textBox1.Multiline;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.textBox1.Location = new Point(10,10);
            this.Height = this.textBox1.Height + 20;
            this.textBox1.Width = this.Width - 20;
            e.Graphics.FillRectangle(this.brush,new Rectangle(5,5,this.Width-5,this.Height-5));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (this.TextBoxTextChanged != null)
            {
                this.TextBoxTextChanged(this,e);
            }
            CheckContent();
            this.Refresh();
        }

        private bool CheckNumber(string str)
        {
            try
            {
                int.Parse(str.Trim());
                return true;
            }
            catch (Exception e) when (e is ArgumentNullException | e is FormatException | e is OverflowException)
            {
                return false;
            }
        }

        private bool CheckTextual(string str)
        {
            bool valid = true;
            for (int i = 0;i < 10 && valid;i++)
            {
                if (!str.Contains(string.Format("{0}",i)))
                {
                    valid = true;
                }
                else
                {
                    valid = false;
                }
            }
            return !CheckNumber(str) && str.Trim().Replace("\\t","").Length > 0 && valid;
        }
    }
}
