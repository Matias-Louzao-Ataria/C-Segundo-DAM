﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.graficosDeBarras1 = new T5DIEj6.GraficosDeBarras();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // graficosDeBarras1
            // 
            this.graficosDeBarras1.AutoSizeAxis = true;
            this.graficosDeBarras1.AxisX = 694;
            this.graficosDeBarras1.AxisY = 336;
            this.graficosDeBarras1.BarsWidth = 100;
            this.graficosDeBarras1.Forma = T5DIEj6.GraficosDeBarras.eForma.Linea;
            this.graficosDeBarras1.Location = new System.Drawing.Point(29, 75);
            this.graficosDeBarras1.Name = "graficosDeBarras1";
            this.graficosDeBarras1.Size = new System.Drawing.Size(696, 338);
            this.graficosDeBarras1.TabIndex = 0;
            this.graficosDeBarras1.Text = "graficosDeBarras1";
            this.graficosDeBarras1.Values = null;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 446);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.graficosDeBarras1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private T5DIEj6.GraficosDeBarras graficosDeBarras1;
        private System.Windows.Forms.Button button1;
    }
}

