﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace DIT5EJ1
{
    [DefaultEvent("Load")]
    [DefaultProperty("Textlbl")]
    public partial class Reproductor : UserControl
    {
        private Bitmap play,pause;

        [Category("1")]
        [Description("seg reaches 59")]
        public event EventHandler DesbordaTiempo;

        [Category("1")]
        [Description("Play button clicked")]
        public event EventHandler botonPulsado;

        public Reproductor()
        {
            InitializeComponent();
            CargarImagenes();

        }

        private int y = 0;
        [Category("1")]
        [Description("Label's content")]
        public int Y
        {
            set
            {
                this.y = value;
                if (this.y == 59 && this.DesbordaTiempo != null)
                {
                    this.DesbordaTiempo(this, new EventArgs());
                }
                Refresh();
            }

            get
            {
                return this.y;
            }
        }

        private int x = 0;

        [Category("1")]
        [Description("Label's content XX")]
        public int X{
            set
            {
                if (value < 99)
                {
                    this.x = value;
                }
                else
                {
                    this.x = 0;
                }
                Refresh();
            }

            get
            {
                return this.x;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            
            this.label1.Text = String.Format("{0,2}:{1,2}",X,Y);
            //recolocar();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            this.Size = this.PreferredSize;
        }


        private void TextlblChanged(Object sender,EventArgs e)
        {
            
        }

        private void Play_Click(object sender, EventArgs e)
        {

            if (this.botonPulsado != null)
            {
                this.botonPulsado(this, new EventArgs());
            }
            if (this.btn.BackgroundImage.Equals(this.pause))
            {
                this.btn.BackgroundImage = this.play;
            }
            else
            {
                this.btn.BackgroundImage = this.pause;
            }
        }

        private void CargarImagenes()
        {
            if (play == null && pause == null)
            {
                this.play = new Bitmap("play.png");
                this.pause = new Bitmap("pause.png");
            }
        }

        private void Reproductor_Load(object sender, EventArgs e)
        {
            
        }

        private void Tick(Object sender,EventArgs e)
        {
            ((System.Windows.Forms.Timer)sender).Stop();
        }
    }
}
