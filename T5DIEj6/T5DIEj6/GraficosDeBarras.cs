﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace T5DIEj6
{
    [DefaultProperty("AnchoBarras")]
    [DefaultEvent("Load")]
    public partial class GraficosDeBarras : Control
    {
        private Brush axisBrush = Brushes.Black;
        private ArrayList overflowBars;

        private bool autoSizeAxis;

        public enum eForma
        {
            Barra,
            Linea
        }

        private eForma forma;

        [Category("1")]
        [Description("What's going to be drawn")]
        public eForma Forma
        {
            set
            {
                this.forma = value;
                this.Refresh();
            }

            get
            {
                return this.forma;
            }
        }

        [Category("1")]
        [Description("Wether or not the axis' size is automatically determined")]
        public bool AutoSizeAxis
        {
            set
            {
                this.autoSizeAxis = value;
                this.Refresh();
            }

            get
            {
                return this.autoSizeAxis;
            }
        }

        private int axisX,axisY;

        [Category("1")]
        [Description("X axis' lenght")]
        public int AxisX
        {
            set
            {
                if (value > 0)
                {
                    this.axisX = value;
                    this.Refresh();
                }
                else
                {
                    throw new ArgumentException();
                }
            }

            get
            {
                return this.axisX;
            }
        }

        [Category("1")]
        [Description("Y axis' lenght")]
        public int AxisY
        {
            set
            {
                if (value > 0)
                {
                    this.axisY = value;
                    this.Refresh();
                }
                else
                {
                    throw new ArgumentException();
                }
            }

            get
            {
                return this.axisY;
            }
        }

        private int barsWidth = 10;

        [Category("1")]
        [Description("Bar's width")]
        public int BarsWidth
        {
            set
            {
                if (value > 0)
                {
                    this.barsWidth = value;
                    this.Refresh();
                }
            }

            get
            {
                return this.barsWidth;
            }
        }

        private ArrayList values;

        public ArrayList Values
        {
            set
            {
                if (value != null)
                {
                    this.values = value;
                    this.Refresh();
                }
            }

            get
            {
                return this.values;
            }
        }

        public GraficosDeBarras()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            string xstr = "X axis",ystr = "Y axis";
            SizeF XstrSize = e.Graphics.MeasureString(xstr, this.Font);
            SizeF YstrSize = e.Graphics.MeasureString(ystr, this.Font);
            Pen p = new Pen(this.axisBrush);
            p.SetLineCap(System.Drawing.Drawing2D.LineCap.Flat,System.Drawing.Drawing2D.LineCap.ArrowAnchor, System.Drawing.Drawing2D.DashCap.Flat);
            e.Graphics.TranslateTransform(YstrSize.Height,YstrSize.Width);
            e.Graphics.RotateTransform(-90);
            e.Graphics.DrawString(ystr,this.Font,Brushes.Black,0,0);
            e.Graphics.ResetTransform();
            e.Graphics.DrawString(xstr,this.Font,Brushes.Black, YstrSize.Width, this.Height - XstrSize.Height);

            if (this.autoSizeAxis || (this.AxisX <= 0 || this.AxisY <= 0))
            {
                //this.Width = e.ClipRectangle.Width;
                //this.Height = e.ClipRectangle.Height;
                if (this.Height < this.axisY ||this.Width < this.axisX)
                {
                    this.Width = this.axisX + 2;
                    this.Height = this.axisY + 2;
                }

                e.Graphics.DrawLine(p, YstrSize.Width, this.Height - XstrSize.Height, YstrSize.Width, 1);
                e.Graphics.DrawLine(p, YstrSize.Width, this.Height - XstrSize.Height, this.Width, this.Height - XstrSize.Height);
            }
            else 
            {
                this.Width = this.axisX + 2;
                this.Height = this.axisY +2;

                e.Graphics.DrawLine(p, YstrSize.Width, this.Height - XstrSize.Height, YstrSize.Width, this.Height - this.AxisY);
                e.Graphics.DrawLine(p, YstrSize.Width, this.Height - XstrSize.Height, this.axisX, this.Height - XstrSize.Height);
            }
            p.Dispose();

            if (this.values != null && this.values.Count > 0)
            {
                int offset = 1;
                int contBrushes = 0;
                Brush[] brushes = { Brushes.Red, Brushes.Blue,Brushes.Yellow};

                if (this.autoSizeAxis)
                {
                    int prediccion = this.values.Count * this.barsWidth + 2;
                    while (prediccion >= this.Width + 2)
                    {
                        this.barsWidth -= 10;
                        prediccion = this.values.Count * this.barsWidth + 2;
                    }

                    foreach (int i in this.values)
                    {
                        if (i > this.Height)
                        {
                            this.axisY = i + 2;
                        }
                    }

                }
                else
                {
                    this.overflowBars = new ArrayList();
                    foreach (int i in this.values)
                    {
                        if (i > this.Height)
                        {
                            this.overflowBars.Add(i);
                        }
                    }
                }

                foreach (int i in this.values)
                {
                    if (this.forma == eForma.Barra)
                    {
                        if (!this.autoSizeAxis && this.overflowBars.Contains(i))
                        {
                            e.Graphics.FillRectangle(Brushes.Red, new Rectangle((int)(XstrSize.Width + offset), (this.Height - (int)XstrSize.Height) - i - 1, this.barsWidth, i));
                        }
                        else
                        {
                            e.Graphics.FillRectangle(brushes[contBrushes], new Rectangle((int)(XstrSize.Width + offset), (this.Height - (int)XstrSize.Height) - i - 1, this.barsWidth, i));
                        }
                    }
                    else
                    {
                        if (!this.autoSizeAxis && this.overflowBars.Contains(i))
                        {
                            e.Graphics.DrawLine(new Pen(Brushes.Red),new PointF(YstrSize.Width + offset, this.Height - XstrSize.Height), new PointF(YstrSize.Width + offset,this.Height - XstrSize.Height-i));
                        }
                        else
                        {
                            e.Graphics.DrawLine(new Pen(brushes[contBrushes]),new PointF(YstrSize.Width+offset,this.Height - XstrSize.Height),new PointF(YstrSize.Width+offset, this.Height - XstrSize.Height - i));
                        }
                    }

                    offset += this.barsWidth + 2;
                    contBrushes++;
                    if (contBrushes >= brushes.Length)
                    {
                        contBrushes = 0;
                    }
                    if (this.Width < offset)
                    {
                        this.Width = offset + this.barsWidth;
                    }
                }
                //e.Graphics.ResetTransform();
            }


        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.axisX = this.Width - 2;
            this.AxisY = this.Height - 2;
            this.Refresh();
        }
    }
}
