﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT5EJ1
{
    [DefaultEvent("Load")]
    [DefaultProperty("Text")]
    public partial class EtiquetaAviso : Control
    {
        [Category("1")]
        [Description("Clicked on eMarca")]
        public event EventHandler ClickEnMarca;
        
        public enum eAdorno
        {
            Cruz,Circulo,Imagen,Nada
        }

        private eAdorno adorno = eAdorno.Nada;

        private int grosor = 0 ;

        private int offsetX = 0;

        [Category("1")]
        [Description("Adorno a la izquierda del componente")]
        public eAdorno Adorno
        {
            set
            {
                this.adorno = value;
                Refresh();
            }

            get
            {
                return this.adorno;
            }
        }

        private Color gradientStart, gradientEnd;

        [Category("1")]
        [Description("Initial color of the background gradient")]
        public Color GradientStart
        {
            set
            {
                if (value != null)
                {
                    this.gradientStart = value;
                    this.Refresh();
                }
                else
                {
                    throw new ArgumentNullException();
                }
            }

            get
            {
                return this.gradientStart;
            }
        }

        [Category("1")]
        [Description("End color of the background gradient")]
        public Color GradientEnd
        {
            set
            {
                if (value != null)
                {
                    this.gradientEnd = value;
                    this.Refresh();
                }
                else
                {
                    throw new ArgumentNullException();
                }
            }

            get
            {
                return this.gradientEnd;
            }
        }

        private bool gradient = false;

        [Category("1")]
        [Description("Enables or disables background gradient")]
        public bool Gradient
        {
            set
            {
                this.gradient = value;
                this.Refresh();
            }

            get
            {
                return this.gradient;
            }
        }

        private Bitmap imagenMarca;

        [Category("1")]
        [Description("Image that's going to be displayed near the component")]
        public Bitmap ImagenMarca
        {
            set
            {
                try
                {
                    Bitmap b = new Bitmap(value);
                    this.imagenMarca = value;
                    this.Refresh();
                }
                catch (Exception ex) when (ex is ArgumentException || ex is ArgumentNullException || ex is NullReferenceException)
                {
                   throw ex;
                }
                /*if (value != null)
                {
                }
                else
                {
                    throw new ArgumentNullException();
                }*/
            }

            get
            {
                return this.imagenMarca;
            }
        }

        public EtiquetaAviso()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            Graphics g = e.Graphics;
            grosor = 0; //Grosor de las líneas de dibujo
            offsetX = 0; //Desplazamiento a la derecha del texto
            int offsetY = 0; //Desplazamiento hacia abajo del texto
                             //Esta propiedad provoca mejoras en la apariencia o en la eficiencia
                             // a la hora de dibujar
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //Dependiendo del valor de la propiedad marca dibujamos una
            //Cruz o un Círculo

            if (this.gradient)
            {
                LinearGradientBrush brush = new LinearGradientBrush(new PointF(0, 0), new PointF(Width, Height), this.gradientStart, this.gradientEnd);
                g.DrawLine(new Pen(brush, this.Height), 0, Height / 2, Width, Height / 2);
                brush.Dispose();
            }

            switch (this.adorno)
            {
                case eAdorno.Circulo:
                    grosor = 20;
                    g.DrawEllipse(new Pen(Color.Green, grosor), grosor, grosor,

                    this.Font.Height, this.Font.Height);

                    offsetX = this.Font.Height + grosor;
                    offsetY = grosor;
                    break;
                case eAdorno.Cruz:
                    grosor = 5;
                    Pen lapiz = new Pen(Color.Red, grosor);
                    g.DrawLine(lapiz, grosor, grosor, this.Font.Height,
                    this.Font.Height);
                    g.DrawLine(lapiz, this.Font.Height, grosor, grosor,
                    this.Font.Height);
                    offsetX = this.Font.Height + grosor;
                    offsetY = grosor / 2;
                    //Es recomendable liberar recursos de dibujo pues se

                    //pueden realizar muchos y cogen memoria

                    lapiz.Dispose();
                    break;

                case eAdorno.Imagen:
                    Console.WriteLine(this.imagenMarca);
                    if (this.imagenMarca != null)
                    {
                        float imgAspectRatio = this.imagenMarca.Width / this.imagenMarca.Height;
                        float height = e.Graphics.MeasureString(this.Text,this.Font).Height;
                        e.Graphics.DrawImage(this.imagenMarca, new RectangleF(0, height, height * imgAspectRatio, height));
                        offsetX = (int)height;
                        offsetY = (int)height;
                    }
                    break;
            }
            //Finalmente pintamos el Texto; desplazado si fuera necesario
            SolidBrush b = new SolidBrush(this.ForeColor);
            g.DrawString(this.Text, this.Font, b, offsetX + grosor, offsetY);
            Size tam = g.MeasureString(this.Text, this.Font).ToSize();
            this.Size = new Size(tam.Width + offsetX + grosor, tam.Height + offsetY* 2);
            b.Dispose();
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            Refresh();
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
        //    Refresh();
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            base.OnForeColorChanged(e);
            Refresh();
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (this.adorno != eAdorno.Nada)
            {
                if (e.X >= 0 && e.X <= this.grosor+offsetX)
                {
                    if (this.ClickEnMarca != null)
                    {
                        this.ClickEnMarca(this,new EventArgs());
                    } 
                }
            }
        }
    }
}
