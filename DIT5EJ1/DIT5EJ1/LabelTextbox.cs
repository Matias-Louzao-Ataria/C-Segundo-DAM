﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIT5EJ1
{

    [
        DefaultProperty("TextLbl"),
        DefaultEvent("Load")
    ]

    public partial class LabelTextbox : UserControl
    {
        [Category("Cambia la colocación")]
        [Description("Cambia el posicionamiento de los componentes internos")]
        public event EventHandler CambiaPosicion;

        [Category("Cambia la colocación")]
        [Description("Cambia la separación de los componentes internos")]
        public event EventHandler CambiarSeparacion;

        [Category("Text")]
        [Description("Cambio del etxto de txt")]
        public event EventHandler TxtTextChanged;

    

        [Category("1")]
        [Description("Cambia el passwdChar del txt")]
        public char PasswdChar
        {
            set
            {
                this.txt.PasswordChar = value;
            }

            get
            {
                return this.txt.PasswordChar;
            }
        }

        public enum ePosicion
        {
            IZQUIERDA, DERECHA
        }

        private ePosicion posicion = ePosicion.IZQUIERDA;

        [Category("Appearance")]
        [Description("Indica si la Label se sitúa a la IZQUIERDA o DERECHA del Textbox")]
        public ePosicion Posicion
        {
            set
            {
                if (Enum.IsDefined(typeof(ePosicion), value))
                {
                    posicion = value;
                    recolocar();
                    if (CambiaPosicion != null)
                    {
                        this.CambiaPosicion(this,new EventArgs());
                        Refresh();
                    }
                }
                else
                {
                    throw new InvalidEnumArgumentException();
                }
            }
            get
            {
                return posicion;
            }
        }

        private int separacion = 0;
        [Category("Design")]
        [Description("Píxels de separación entre Label y Textbox")]
        public int Separacion
        {
            set
            {
                if (value >= 0)
                {
                    separacion = value;
                    
                    if (CambiarSeparacion != null)
                    {
                        CambiarSeparacion(this,new EventArgs());
                        Refresh();
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            get
            {
                return separacion;
            }
        }

        [Category("Appearance")]
        [Description("Texto asociado a la Label del control")]
        public string TextLbl
        {
            set
            {
                lbl.Text = value;
            }
            get
            {
                return lbl.Text;
            }
        }

        [Category("Appearance")]
        [Description("Texto asociado al TextBox del control")]
        public string TextTxt
        {
            set
            {
                txt.Text = value;
            }
            get
            {
                return txt.Text;
            }
        }

        public LabelTextbox()
        {
            InitializeComponent();
            TextLbl = Name;
            TextTxt = "";
        }

        private void recolocar()
        {
            switch (posicion)
            {
                case ePosicion.DERECHA:
                    //Establecemos posición del componente txt
                    txt.Location = new Point(0, 0);
                    //Establecemos ancho del Textbox (la label tiene ancho fijo)
                    txt.Width = this.Width - lbl.Width - Separacion;
                    //Establecemos posición del componente lbl
                    lbl.Location = new Point(txt.Width + Separacion, 0);
                    //Establecemos altura del componente
                    this.Height = Math.Max(txt.Height, lbl.Height);
                    break;
                case ePosicion.IZQUIERDA:
                    lbl.Location = new Point(0, 0);
                    txt.Location = new Point(lbl.Width + Separacion, 0);
                    txt.Width = this.Width - lbl.Width - Separacion;
                    this.Height = Math.Max(txt.Height, lbl.Height);
                    break;
            }
        }
        // Esta función has de enlazarla con el evento SizeChanged.
        // Sería necesario también tener en cuenta otros eventos como FontChanged
        // que aquí nos saltamos.
        private void LabelTextBox_SizeChanged(object sender, EventArgs e)
        {
            
        }

        private void Txt_KeyUp(object sender, KeyEventArgs e)
        {
            Console.WriteLine("TXT KeyUp");
            OnKeyUp(e);
        }

        private void LabelTextBoxKeyUp(Object sender,KeyEventArgs e)
        {
            Console.WriteLine("LabelTextBox keyUp!!!");
        }

        private void Txt_TextChanged(object sender, EventArgs e)
        {
            if (this.TxtTextChanged != null)
            {
                TxtTextChanged(sender,e);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawLine(new Pen(Brushes.Yellow),new Point(this.lbl.Location.X, this.Height - 1),new Point(this.lbl.Location.X+this.lbl.Width,this.Height-1));
            recolocar();
        }
    }
}
